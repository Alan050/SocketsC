#include<sys/socket.h>
#include<arpa/inet.h>
#include<netinet/in.h>
#include<netdb.h>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<unistd.h>


int main(int argc, char **argv){
  if(argc<2){ 
    printf("%s <puerto>\n",argv[0]);
    return 1;
  }

  struct sockaddr_in servidor, cliente;
  char buffer[100]; 
  puerto = atoi(argv[1]);

  socklen_t longAux; 
  int conexionServidor, conexionCliente, puerto; 
  
  conexionServidor = socket(AF_INET, SOCK_STREAM, 0); 
  bzero((char *)&servidor, sizeof(servidor)); 

  servidor.sin_family = AF_INET; 
  servidor.sin_port = htons(puerto);
  servidor.sin_addr.s_addr = INADDR_ANY;

  if(bind(conexionServidor, (struct sockaddr *)&servidor, sizeof(servidor)) < 0){ 
    printf("Error al vincular el puerto :C\n");
    close(conexionServidor);
    return 1;
  }

  listen(conexionServidor, 3); 
  printf("El puerto escuchando: %d\n", ntohs(servidor.sin_port));
  longAux = sizeof(cliente); 
  conexionCliente = accept(conexionServidor, (struct sockaddr *)&cliente, &longAux); 

  if(conexionCliente<0){
    printf("Error \n");
    close(conexionServidor);
    return 1;
  }

  printf("Conectando con %s:%d\n", inet_ntoa(cliente.sin_addr),htons(cliente.sin_port));

  if(recv(conexionCliente, buffer, 100, 0) < 0){ 
    printf("Error al recibir los datos\n");
    close(conexionServidor);
    return 1;

  }else{

    printf("%s\n", buffer);
    bzero((char *)&buffer, sizeof(buffer));
    send(conexionCliente, "Recibido \n", 13, 0);
  }

  close(conexionServidor);
  return 0;
}